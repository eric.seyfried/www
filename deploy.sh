#!/bin/sh

if [ -d "/var/www" ]
then
        cd /var/www
        git pull
else
        git clone https://gitlab.com/eric.seyfried/www.git
fi